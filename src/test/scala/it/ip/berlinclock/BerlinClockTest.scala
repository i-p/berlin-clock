package it.ip.berlinclock

import org.scalatest.Matchers
import org.scalatest.FreeSpec
import org.scalatest.prop.TableDrivenPropertyChecks._

class BerlinClockTest extends FreeSpec with Matchers {
  val clock = new BerlinClock
  val newline = System.lineSeparator()
  
  "BerlinClock is a fancy way to represent time - https://en.wikipedia.org/wiki/Mengenlehreuhr" - {
    "Our converter will show how regular time will be displayed at BerlinClock. Program input is time in 24 hour format with two digits for each time unit separated by semicolon" - {
      "It should be exactly two digits per time unit" - {
        "One digit for hours, like in 0:00:00 - is incorrect" in {
          intercept[IllegalArgumentException] {
            clock.convert("0:00:00")
          }
        }
        "Similar for minutes - 00:0:00" in {
        	intercept[IllegalArgumentException] {
        		clock.convert("00:0:00")
        	}
        }
        "And seconds - 00:00:0" in {
        	intercept[IllegalArgumentException] {
        		clock.convert("00:00:0")
        	}
        }
        "The same is about extra digits" in {
          intercept[IllegalArgumentException] {
        		clock.convert("00:00:000")
        	}
        }
        "And no one cares about your milliseconds" in {
          intercept[IllegalArgumentException] {
        		clock.convert("00:00:00.000")
        	}
        }
      }
      "Separators other than semilocon are not honored" in {
        intercept[IllegalArgumentException] {
      		clock.convert("00.00.00")
      	}
      }
      "Obviously you should use only digits" in {
        intercept[IllegalArgumentException] {
      		clock.convert("aa:aa:aa")
      	}
      }
      "And all digits expected to be within thresholds" - {
        "Not negative" in {
          intercept[IllegalArgumentException] {
      		  clock.convert("-10:00:00")
      	  }
        }
        "And 'not too positive' (? 25:00:00) either" in {
          intercept[IllegalArgumentException] {
      		  clock.convert("25:00:00")
      	  }
        }
        "Minutes joint next hour" in {
          intercept[IllegalArgumentException] {
      		  clock.convert("00:60:00")
      	  }
        }
        "And seconds - next minute" in {
          intercept[IllegalArgumentException] {
      		  clock.convert("00:00:60")
      	  }
        }
      }
    }
  }


  val startOfDayState = List("O",
                             "OOOO",
                             "OOOO",
                             "OOOOOOOOOOO",
                             "OOOO").mkString(newline)

  val endOfDayState = List("Y", 
                           "RRRR",
                           "RRRO",
                           "YYRYYRYYRYY",
                           "YYYY").mkString(newline)

  val _16_50_01 = List("Y",
                       "RRRO",
                       "ROOO",
                       "YYRYYRYYRYO",
                       "OOOO").mkString(newline)

  val _06_06_01 = List("Y", 
            				   "ROOO",
            				   "ROOO",
            				   "YOOOOOOOOOO",
            				   "YOOO").mkString(newline)

  val examples = Table(("Time",     "BerlinClock"),
                       ("00:00:00", startOfDayState),
                       ("23:59:59", endOfDayState),
                       ("16:50:01", _16_50_01),
                       ("06:06:01", _06_06_01))

  "But if you will provide everything correctly the clock will reward you with something that is hard to read" in {
    forAll(examples)((time, clockState) => {
      println(time + newline + "=" + newline + clockState + newline)
      clock.convert(time) should be (clockState)})
  }
}