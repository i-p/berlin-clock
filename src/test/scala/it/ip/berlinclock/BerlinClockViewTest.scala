package it.ip.berlinclock

import org.scalatest.FreeSpec
import org.scalatest.Matchers

class BerlinClockViewTest extends FreeSpec with Matchers {
  val newline = System.lineSeparator()
  val view = new BerlinClockView()

  val minimumLightsState = List(0,0,0,0,0)
  val maximumLightsState = List(1, 4, 4, 11, 4) // incorrect time with almost 25 hours

  val BerlinClockAllLit = BerlinClockView.ClockCellsLitRepresentation.mkString(newline)
  val BerlincClockAllOff = BerlinClockAllLit.replaceAll(".", BerlinClockView.ClockCellOffSymbol)

  "Let's start with negative cases" - {
	  "null data list should result in exception" in {
		  intercept[IllegalArgumentException] {
			  view.getDataDisplayed(null)
		  }
	  }
	  
	  "Empty data list should result in exception" in {
		  intercept[IllegalArgumentException] {
			  view.getDataDisplayed(Nil)
		  }
	  }
	  
	  "Shorter than expected data list should result in exception" in {
		  intercept[IllegalArgumentException] {
			  view.getDataDisplayed(minimumLightsState.drop(1))
		  }
	  }
	  
	  "Longer than expected data list should result in exception" in {
		  intercept[IllegalArgumentException] {
			  view.getDataDisplayed(0::minimumLightsState)
		  }
	  }

	  "Negative values in any of position should result in exception" - {
		  "in seconds" in {
			  intercept[IllegalArgumentException] {
				  view.getDataDisplayed(List(-1,0,0,0,0))
			  }
		  }
		  
		  "in hour groups" in {
			  intercept[IllegalArgumentException] {
				  view.getDataDisplayed(List(0,-1,0,0,0))
			  }
		  }
		  
		  "in hours" in {
			  intercept[IllegalArgumentException] {
				  view.getDataDisplayed(List(0,0,-1,0,0))
			  }
		  }
		  
		  "in minute groups" in {
			  intercept[IllegalArgumentException] {
				  view.getDataDisplayed(List(0,0,0,-1,0))
			  }
		  }
		  
		  "in minutes" in {
			  intercept[IllegalArgumentException] {
				  view.getDataDisplayed(List(0,0,0,0,-1))
			  }
		  }
	  }
  }

  "And now take a look at some valid examples" - {
	  "All zeros data list should result in BerlinClock's all cells off" in {
		  view.getDataDisplayed(minimumLightsState) should be (BerlincClockAllOff)
	  }
	  
	  "Maximum count in each row should result in BerlinClock's all cells are lit" in {
		  view.getDataDisplayed(maximumLightsState) should be (BerlinClockAllLit)
	  }
	  
	  "Values more than maximum in each row are ignored without errors" in {
		  view.getDataDisplayed(List(255, 255, 255, 255, 255)) should be (BerlinClockAllLit)
	  }
	  
	  "All ones data list should result in BerlinClock's only first cell in each row lit" in {
		  view.getDataDisplayed(List(1,1,1,1,1)) should be (List("Y", 
				  "ROOO",
				  "ROOO",
				  "YOOOOOOOOOO",
				  "YOOO").mkString(newline))
	  }
  }
}