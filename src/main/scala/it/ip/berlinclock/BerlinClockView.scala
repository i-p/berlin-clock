package it.ip.berlinclock

object BerlinClockView {
	val ClockCellsLitRepresentation = List("Y", 
			"RRRR",
			"RRRR",
			"YYRYYRYYRYY",
			"YYYY")
	val ClockCellOffSymbol = "O"
}

class BerlinClockView {
  def getDataDisplayed(clockData: List[Int]) = {
    if(clockData == null ||
        clockData.isEmpty ||
        clockData.size != BerlinClockView.ClockCellsLitRepresentation.size ||
        !clockData.forall { _ >= 0 })
      throw new IllegalArgumentException("View input is not as expected. You'll have to dig into the code to find out.")

    BerlinClockView.ClockCellsLitRepresentation.zip(clockData).map(element => {
      val lightsLine = element._1
      val litLightsCount = element._2
      lightsLine.take(litLightsCount) + BerlinClockView.ClockCellOffSymbol*(lightsLine.size - litLightsCount)
    }).mkString(System.lineSeparator).trim()
  }
}