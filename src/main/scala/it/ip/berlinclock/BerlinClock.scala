package it.ip.berlinclock

class BerlinClock {
  def convert(time:String):String = {
    validate(time)
    val (hours, minutes, seconds) = parseDigits(time)
    val clockData:List[Int] = countLightsForEachRow(hours, minutes, seconds)
    new BerlinClockView().getDataDisplayed(clockData)
  }

  private def validate(time:String) {
    val ValidInputPattern = "([01]?[0-1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]"

    if(time == null || !time.matches(ValidInputPattern))
      wrongInput(time)
  }

  private def parseDigits(time:String) = {
    val TimeDigitsSeparator = ":"

    val digits:List[Int] = time.split(TimeDigitsSeparator).toList.map(_.toInt)
    digits match {
      case hours::minutes::seconds::Nil => (hours,minutes,seconds)
      case _ => wrongInput(time)
    }
  }

  def wrongInput(time: String) = {
    throw new IllegalArgumentException(s"Invalid input - '$time'. Input should represent valid time in 24 hours format, e.g. - '12:34:56'")
  }

  private def countLightsForEachRow(hours: Int, minutes: Int, seconds: Int) = {
    val TimeUnitsGroupSize = 5

    List(seconds % 2,
        hours / TimeUnitsGroupSize,
        hours % TimeUnitsGroupSize,
        minutes / TimeUnitsGroupSize,
        minutes % TimeUnitsGroupSize) 
  }
}