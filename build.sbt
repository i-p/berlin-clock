lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1"

lazy val root = (project in file("."))
  .settings(
    organization := "com.ip",
    name := "berlin-clock",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := "2.12.2",
    libraryDependencies += scalaTest % Test
  )
